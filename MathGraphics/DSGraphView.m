//
//  DSGraphView.m
//  MathGraphics
//
//  Created by Denis Andreev on 04.09.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import "DSGraphView.h"

@implementation DSGraphView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    // Drawing code here.
    [[NSColor controlShadowColor] set];
    NSRectFill(dirtyRect);
    [super drawRect:dirtyRect];
}

-(void)clearView
{
    [self setNeedsDisplay:YES];
    [self display];
}

@end
