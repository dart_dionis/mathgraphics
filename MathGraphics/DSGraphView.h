//
//  DSGraphView.h
//  MathGraphics
//
//  Created by Denis Andreev on 04.09.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DSGraphView : NSView
-(void)clearView;
@end
