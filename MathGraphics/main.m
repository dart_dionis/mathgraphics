//
//  main.m
//  MathGraphics
//
//  Created by Denis Andreev on 27.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
