//
//  DSAppDelegate.m
//  MathGraphics
//
//  Created by Denis Andreev on 27.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import "DSAppDelegate.h"

@implementation DSAppDelegate

@synthesize graph; // custon view class
@synthesize grPath; // NSBezierPath
@synthesize xValue;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [self setGraphLines];
}


- (void)setGraphLines // x and y lines
{

    [graph lockFocus];
    int w = graph.frame.size.width;
    int h = graph.frame.size.height;
    
    [grPath setLineWidth:2];
    grPath = [NSBezierPath bezierPath];
    // line y
    NSPoint startPoint = {w/2, 0};
    NSPoint endPoint = {w/2, h-10};
    [grPath moveToPoint:startPoint];
    [grPath lineToPoint:endPoint];
    // line x
    NSPoint hline = {5, h/2};
    NSPoint xline = {w-20, h/2};
    [grPath moveToPoint:hline];
    [grPath lineToPoint:xline];
    
    // for x
    for (float i=w/2; i<w-35; i+=23.5) {
        NSPoint startLines = {i, h/2+3};
        NSPoint endLines = {i, h/2-3};
        [grPath moveToPoint:startLines];
        [grPath lineToPoint:endLines];
    }
    for (int i=w/2; i>0; i-=23.5) {
        NSPoint startLines = {i, h/2+3};
        NSPoint endLines = {i, h/2-3};
        [grPath moveToPoint:startLines];
        [grPath lineToPoint:endLines];
    }
    // for y
    for (int i=h/2; i<h-25; i+=23.5) {
        NSPoint startLines = {w/2+3, i};
        NSPoint endLines = {w/2-3, i};
        [grPath moveToPoint:startLines];
        [grPath lineToPoint:endLines];
    }
    for (int i=h/2; i>0; i-=23.5) {
        NSPoint startLines = {w/2+3, i};
        NSPoint endLines = {w/2-3, i};
        [grPath moveToPoint:startLines];
        [grPath lineToPoint:endLines];
    }
    //x
    NSPoint xPis = {w-20, h/2};
    NSPoint xPie = {w-35, h/2+3};
    [grPath moveToPoint:xPis];
    [grPath lineToPoint:xPie];
    
    NSPoint xPis1 = {w-20, h/2};
    NSPoint xPie1 = {w-35, h/2-3};
    [grPath moveToPoint:xPis1];
    [grPath lineToPoint:xPie1];
    ////y
    NSPoint yPis = {w/2, h-10};
    NSPoint yPie = {w/2+3, h-25};
    [grPath moveToPoint:yPis];
    [grPath lineToPoint:yPie];
    
    NSPoint yPis1 = {w/2, h-10};
    NSPoint yPie1 = {w/2-3, h-25};
    [grPath moveToPoint:yPis1];
    [grPath lineToPoint:yPie1];
    
    [[NSColor blueColor] set];
    [grPath fill];
    [grPath stroke];
    [graph unlockFocus];
}


-(void)creategraphFunk:(float)x
{
    int w = graph.frame.size.width;
    int h = graph.frame.size.height;

    [graph lockFocus];

    grPath = [NSBezierPath bezierPath];
    [grPath setLineWidth:2];
    
    NSPoint startGr = {w/2, h/2};
    [grPath moveToPoint:startGr];
  
    for (float i=0; i<=x; i+=0.01) {
        float y = sin(i);
        NSPoint endGr = {w/2+i*23.5, h/2+y*23.5};
        [grPath lineToPoint:endGr];
    }

    [[NSColor redColor] set];
    [grPath stroke];
    [graph unlockFocus];
}



- (IBAction)createGraph:(id)sender {
    [graph clearView];
    [self setGraphLines];
    float x = xValue.floatValue;
    [self creategraphFunk:x];
}
@end
