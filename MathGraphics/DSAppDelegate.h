//
//  DSAppDelegate.h
//  MathGraphics
//
//  Created by Denis Andreev on 27.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DSGraphView.h"

@interface DSAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet DSGraphView *graph;
@property (nonatomic, retain) NSBezierPath *grPath;
@property (weak) IBOutlet NSTextField *xValue;

- (IBAction)createGraph:(id)sender;
- (void)setGraphLines;
- (void)creategraphFunk:(float)x;

@end
